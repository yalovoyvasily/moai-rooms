using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    private enum State
    {
        Close,
        Opening,
        Open,
        Closing
    }

    State state;

    private Rigidbody rb;

    public int speed;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Quaternion CloseRotation = Quaternion.Euler(new Vector3(0,0, speed) * Time.fixedDeltaTime);
        Quaternion OpenRotation = Quaternion.Euler(new Vector3(0, 0, -speed) * Time.fixedDeltaTime);

        Debug.Log(Time.fixedDeltaTime);
        


        switch (state)
        {
            case State.Close:
                
                if (Input.GetKey(KeyCode.E))
                {
                    rb.constraints = RigidbodyConstraints.None;
                    state = State.Opening;
                }
                break;
            case State.Opening:
                //if (rb.rotation.y <= -0.7)
                //    rb.AddForce(new Vector3(0, 0, -5));
                //else
                //    rb.AddForce(new Vector3(0, 0, -50));
                rb.velocity = new Vector3(0, 0, -10);
                if (rb.rotation.y <= -0.8)
                {
                    Debug.Log("���");
                    rb.constraints = RigidbodyConstraints.FreezeAll;
                    state = State.Open;
                }
                
                break;
            case State.Open:
                if (Input.GetKey(KeyCode.E))
                {
                    rb.constraints = RigidbodyConstraints.None;
                    state = State.Closing;
                }
                break;
            case State.Closing:

                //if (rb.rotation.y >= -0.4)
                //    rb.AddForce(new Vector3(0, 0, 5));
                //else
                //    rb.AddForce(new Vector3(0, 0, 50));
                rb.velocity = new Vector3(0, 0, 10);

                if (rb.rotation.y >= 0)
                {
                    rb.constraints = RigidbodyConstraints.FreezeAll;
                    state = State.Close;
                }
                break;

                
        }
        Debug.Log(rb.rotation.y);
        Debug.Log(state);

        //if (Input.GetKey(KeyCode.W))
        //    rb.AddForce(new Vector3(0, 0, 10));
        //if (Input.GetKey(KeyCode.S))
        //    rb.AddForce(new Vector3(0, 0, -10));

        //if (rb.rotation.y <= 0 && Input.GetKey(KeyCode.K))
        //    rb.constraints = RigidbodyConstraints.FreezeAll;
        //else
        //    rb.constraints = RigidbodyConstraints.None;
    }
}
