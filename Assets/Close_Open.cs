using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Close_Open : MonoBehaviour
{
    public bool IsOpen;

    public int x, y, z;

    private int State;
    Rigidbody Door;
    // Start is called before the first frame update
    void Start()
    {
        Door = GetComponent<Rigidbody>();
        //Set the angular velocity of the Rigidbody (rotating around the Y axis, 100 deg/sec)
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (IsOpen)
        Door.MovePosition(new Vector3(x, y, z));
        //BroadcastMessage.Broadcast()
    }
}
